package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger(MyView.class);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Tasks2(3) - Create your own annotation...");
        menu.put("2", "  2 - Tasks4 - Invoke three methods");
        menu.put("3", "  3 - Tasks5 - Set value into field not knowing its type");
        menu.put("4", "  4 - Tasks6 - Invoke myMethod(String a, int ... args) and myMethod(String … args)");
        menu.put("5", "  5 - Tasks7 - Show information about class using class that receive object of unknown type");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::testAnnotation);
        methodsMenu.put("2", this::invokeThreeMethods);
        methodsMenu.put("3", this::setField);
        methodsMenu.put("4", this::invokeMyMethods);
        methodsMenu.put("5", this::showInfo);
    }

    private void testAnnotation() {
        controller.testTask2();
    }

    private void invokeThreeMethods() {
        controller.invokeMethods();
    }

    private void setField() {
        logger.info("Test new value: " + controller.setField());
    }

    private void invokeMyMethods() {
        controller.invokeMyMethods();
    }

    private void showInfo() {
        controller.showInfo();
    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
//                logger.info("Wrong input!");
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}
