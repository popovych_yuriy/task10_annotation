package com.epam.controller;

import com.epam.model.BusinessLogic;
import com.epam.model.Model;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public void testTask2() {
        model.testTask2();
    }

    @Override
    public void invokeMethods() {
        model.invokeMethods();
    }

    @Override
    public String setField() {
        return model.setField();
    }

    @Override
    public void invokeMyMethods() {
        model.invokeMyMethods();
    }

    @Override
    public void showInfo() {
        model.showInfo();
    }
}