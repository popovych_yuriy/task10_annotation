package com.epam.controller;

public interface Controller {
    void testTask2();

    void invokeMethods();

    String setField();

    void invokeMyMethods();

    void showInfo();
}
