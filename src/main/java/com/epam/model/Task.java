package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class Task {
    private static Logger logger = LogManager.getLogger(Task.class);

    private String taskName = "Annotation";

    private int testEmpty;

    @CustomAnnotation
    private int test = 42;

    @CustomAnnotation(custom = 10)
    private int custom;

    public void testTask() {
        Task task = new Task();
        Class clazz = task.getClass();

        logger.info("Fields: ");
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            logger.info(field.getName());
            logger.info(field.getType().getName());
            logger.info(Modifier.toString(field.getModifiers()));

            CustomAnnotation annotation = field.getAnnotation(CustomAnnotation.class);
            if (annotation != null) {
                logger.info("Value of field from annotation: " + annotation.custom());
            }
        }
    }

    public int method1() {
        logger.info("Method1 invoked");
        return test;
    }

    public String method2() {
        logger.info("Method2 invoked");
        return taskName;
    }

    public String[] method3(String value1, String value2) {
        logger.info("Method3 invoked");
        return new String[]{value1, value2};
    }

    public String getTaskName() {
        return taskName;
    }

    public void myMethod(String... args) {
        logger.info("Invoked myMethod(String ... args)");
    }

    public void myMethod(String a, int... args) {
        logger.info("Invoked myMethod(String a, int ... args)");
    }

}
