package com.epam.model;

public interface Model {
    void testTask2();

    void invokeMethods();

    String setField();

    void invokeMyMethods();

    void showInfo();
}
