package com.epam.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BusinessLogic implements Model {
    Task task1;

    public BusinessLogic() {
        task1 = new Task();
    }

    @Override
    public void testTask2() {
        task1.testTask();
    }

    @Override
    public void invokeMethods() {
        Class clazz = task1.getClass();
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            try {
                method.invoke(task1, null);
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String setField() {
        String value = "";
        Class clazz = task1.getClass();
        try {
            Field taskName = clazz.getDeclaredField("taskName");
            taskName.setAccessible(true);
            taskName.set(task1, "Changed Value");
            value = task1.getTaskName();
            return value;
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return value;
    }

    @Override
    public void invokeMyMethods() {
        try {
            Class clazz = task1.getClass();
            Method methodSingle = clazz.getDeclaredMethod("myMethod", String[].class);
            methodSingle.invoke(task1, new String[1]);
            Method methodDouble = clazz.getDeclaredMethod("myMethod", String.class, int[].class);
            methodDouble.invoke(task1, "", new int[1]);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showInfo() {
        Task7 task7 = new Task7(new Object());
        task7.showInfo();
    }
}
