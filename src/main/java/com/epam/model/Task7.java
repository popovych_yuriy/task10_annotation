package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.*;

public class Task7 {
    private static Logger logger = LogManager.getLogger(Task.class);
    private Object object;

    public Task7(Object object) {
        this.object = object;
    }

    public void showInfo() {
        Class clazz = object.getClass();
        logger.info("Class name: " + clazz.getName());
        showConstructors(clazz);
        showMethods(clazz);
        showFields(clazz);
    }

    private void showConstructors(Class clazz) {
        logger.info("Constructors: ");
        Constructor[] constructors = clazz.getConstructors();
        for (Constructor constructor : constructors) {
            logger.info(" Constructor name: " + constructor.getName());
            Parameter[] parameters = constructor.getParameters();
            for (Parameter parameter : parameters) {
                logger.info("   Parameter name: " + parameter.getName());
                logger.info("   Parameter type: " + parameter.getType().getName());
            }
        }
    }

    private void showMethods(Class clazz) {
        logger.info("Methods: ");
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            logger.info(" Method name: " + method.getName());
            Parameter[] parameters = method.getParameters();
            for (Parameter parameter : parameters) {
                logger.info("   Parameter name: " + parameter.getName());
                logger.info("   Parameter type: " + parameter.getType().getName());
            }
            logger.info("   Method modifiers: " + Modifier.toString(method.getModifiers()));
            logger.info("   Method return type: " + method.getReturnType().getName());
        }
    }

    private void showFields(Class clazz) {
        logger.info("Fields: ");
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            logger.info(" Field name: " + field.getName());
            logger.info("   Field type: " + field.getType().getName());
            logger.info("   Field modifiers: " + Modifier.toString(field.getModifiers()));
        }
    }
}
